package az.ingress.unibootcampsecuritytask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UniBootcampSecurityTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(UniBootcampSecurityTaskApplication.class, args);
	}

}
